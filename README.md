# SureRent
This project aims to produce an api that will allow users to gain free access to data on the rental units in the country.
Users will require to register before they can use the api. The api will be released upon completion of the project  after
a considerable amount of data has been collected. Data will continously be updated as more and more data is collected.

The same api will also allow users to find the monthly, weekly, or daily rental indices calculated on the data set.